import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest';
import {Report} from '../models';
import {ReportRepository} from '../repositories';

export class CasesController {
  constructor(
    @repository(ReportRepository)
    public caseRepository: ReportRepository,
  ) {}

  @post('/reports', {
    responses: {
      '200': {
        description: 'Report model instance',
        content: {'application/json': {schema: getModelSchemaRef(Report)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Report, {
            title: 'NewCase',
            exclude: ['id'],
          }),
        },
      },
    })
    report: Omit<Report, 'id'>,
  ): Promise<Report> {
    return this.caseRepository.create(report);
  }

  @get('/reports/count', {
    responses: {
      '200': {
        description: 'Report model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Report) where?: Where<Report>): Promise<Count> {
    return this.caseRepository.count(where);
  }

  @get('/reports', {
    responses: {
      '200': {
        description: 'Array of Report model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Report, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(Report) filter?: Filter<Report>): Promise<Report[]> {
    return this.caseRepository.find(filter);
  }

  @patch('/reports', {
    responses: {
      '200': {
        description: 'Report PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Report, {partial: true}),
        },
      },
    })
    report: Report,
    @param.where(Report) where?: Where<Report>,
  ): Promise<Count> {
    return this.caseRepository.updateAll(report, where);
  }

  @get('/reports/{id}', {
    responses: {
      '200': {
        description: 'Report model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Report, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Report, {exclude: 'where'})
    filter?: FilterExcludingWhere<Report>,
  ): Promise<Report> {
    return this.caseRepository.findById(id, filter);
  }

  @patch('/reports/{id}', {
    responses: {
      '204': {
        description: 'Report PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Report, {partial: true}),
        },
      },
    })
    report: Report,
  ): Promise<void> {
    await this.caseRepository.updateById(id, report);
  }

  @put('/reports/{id}', {
    responses: {
      '204': {
        description: 'Report PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() report: Report,
  ): Promise<void> {
    await this.caseRepository.replaceById(id, report);
  }

  @del('/reports/{id}', {
    responses: {
      '204': {
        description: 'Report DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.caseRepository.deleteById(id);
  }
}
