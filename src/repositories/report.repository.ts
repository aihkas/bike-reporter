import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {BikereportsDataSource} from '../datasources';
import {CaseRelations, Report} from '../models';

export class ReportRepository extends DefaultCrudRepository<
  Report,
  typeof Report.prototype.id,
  CaseRelations
> {
  constructor(
    @inject('datasources.bikereports') dataSource: BikereportsDataSource,
  ) {
    super(Report, dataSource);
  }
}
