import {DefaultCrudRepository} from '@loopback/repository';
import {Officer, OfficerRelations} from '../models';
import {BikereportsDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class OfficerRepository extends DefaultCrudRepository<
  Officer,
  typeof Officer.prototype.id,
  OfficerRelations
> {
  constructor(
    @inject('datasources.bikereports') dataSource: BikereportsDataSource,
  ) {
    super(Officer, dataSource);
  }
}
