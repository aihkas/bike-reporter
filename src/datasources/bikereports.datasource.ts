import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'bikereports',
  connector: 'postgresql',
  url: 'postgres://postgres:mysuperbikepassword@localhost:5432',
  host: '127.0.0.1',
  port: 5432,
  user: 'postgres',
  password: 'mysuperbikepassword',
  database: 'bikesdb',
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class BikereportsDataSource
  extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'bikereports';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.bikereports', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
