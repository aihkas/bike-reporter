import {Entity, model, property} from '@loopback/repository';

@model()
export class Report extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    default: 'No title',
  })
  title?: string;

  @property({
    type: 'string',
  })
  brand?: string;

  @property({
    type: 'string',
    required: true,
  })
  city: string;

  description?: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @property({
    type: 'date',
    required: true,
  })
  reportedAt: string;

  @property({
    type: 'boolean',
    default: false,
  })
  solved?: boolean;

  @property({
    type: 'number',
  })
  officerId?: number;

  @property({
    type: 'boolean',
  })
  image?: boolean;

  constructor(data?: Partial<Report>) {
    super(data);
  }
}

export interface CaseRelations {
  // describe navigational properties here
}

export type CaseWithRelations = Report & CaseRelations;
