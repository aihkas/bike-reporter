import {Entity, model, property} from '@loopback/repository';

@model()
export class Officer extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: false,
    required: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'number',
  })
  caseNumber?: number;

  @property({
    type: 'boolean',
    default: true,
  })
  available?: boolean;

  constructor(data?: Partial<Officer>) {
    super(data);
  }
}

export interface OfficerRelations {
  // describe navigational properties here
}

export type OfficerWithRelations = Officer & OfficerRelations;
